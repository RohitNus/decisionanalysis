#include <cstdio>
#include <iostream>
#include <set>
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/raw_ostream.h"
#include <llvm/IR/Constants.h>

/*Color Encoding*/
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

using namespace llvm;
struct CFG{
        /* Name of the BB: can be named or unnamed */
        std::string name;
        /* Pointer to the BasiBlock pointing too */
        llvm::BasicBlock* pointer;
        /* Set of children */
        std::set<CFG*> children;
};


/* Interval structure that is used to provide the possible values for a value for the variables */
struct Interval{
	long lowerBound;
	long upperBound;
};

struct ValueLong{
	long min;
	long max;
	long val;
	bool unbounded;
};

void resetLoopInfo();
void markUnboundedVariablesAfterLoop(BasicBlock);
void outputBBToIntervalMap(BasicBlock*);
std::map<Instruction*, Interval*> getLatestMap();
/* New */
std::map<Instruction*, ValueLong*> getFixedPointOfLoop(BasicBlock*, std::map<Instruction*, ValueLong*>);
/* Function prints the separation per instructor. */
void sep(Instruction*, Instruction*);
/* Function prints the separation for all variables. */
void outputSeps();
/* Function that returns true if both intervals are same */
bool isSameInterval(Interval*, Interval*);
void outputvariablesToCrudeIntervalMap();
bool anyChangeinRangeofValues(BasicBlock*);
bool isLoopUnrollLimitReached(BasicBlock*);
bool checkIfFixedPointIsReached(BasicBlock*); 
void addValueToIntervalMap(ValueLong*, Instruction*);
/* This function executes the opcode using the values */
ValueLong* executeExpression(std::string, ValueLong*, ValueLong*);
/* This function prints the value of the variable map*/
void outputvariablesTOValueMap(std::map<Instruction*, ValueLong*>);
/* This function prints the visited blocks */
void printVisitedBlocks(std::set<BasicBlock*>);
/* This function detects all the loops in the program(while loop specifically) */
void detectAllloopBlocks(BasicBlock*, std::set<BasicBlock*>);
/* generate the variablesToValueMap of the command*/
std::map<Instruction*, ValueLong*> differencePassOnBlock(BasicBlock* BB, CFG* parentNode, std::map<Instruction*, ValueLong*> variablesToValueMap);
/* update the variables of the basic block */
std::map<Instruction*, ValueLong*> updateVariablesOfBasicBlock(BasicBlock* BB, std::map<Instruction*, ValueLong*> variablesToValueMap);
/* Takes the map, and adds the value into the map, and returns the update map */
std::map<Instruction*, ValueLong*> addValueToVariable(ValueLong*, Instruction*, std::map<Instruction*, ValueLong*>);
/* Graph operations */
void addNode(CFG* , CFG*);
/* Pretty print the CFG */
void printCFG(CFG*);
/* Gets a new name or an existing name for a basicblock */
std::string getName(BasicBlock*);
/*Gets a new variable name*/
std::string getVariableName(Instruction*);
/* Gets a new CFG node */
CFG* getNode(llvm::BasicBlock*, std::string);
/* BasicBlock set*/
std::set<BasicBlock*> BBSet;
/* BasicBlock to name map*/
std::map<BasicBlock*, std::string> BBtoNameMap;
/* Varible to name map*/
std::map<Instruction*, std::string> instNameMap;
/* BasicBlock to Instruction set*/
std::map<BasicBlock*, std::set<Instruction*>> BBtoInstructionSet;
/* Stores the pointer to the loop blocks*/
std::set<BasicBlock*> loopBlocks;
/* Stores the variable to crude interval map*/
std::map<Instruction*, Interval*> variableToCrudeIntervalMap;
/* Stores the number of times the loop is unrolled */
std::map<BasicBlock*, long> bbToLoopUnRollCounter;
/* Stores the BB to interval map */
std::map<BasicBlock*, std::map<Instruction*, Interval*>> bbToIntervalMap;
/* Is set when the current context is a loop */
bool isLoopContext = true;
/* Loop increase and decrease variable info */
std::map<Instruction*, long> loopIncreaseInfo;

/* variables set */
std::set<Instruction*> variablesSet;

/* This is configurable */
int MAX_NUMBER_OF_LOOP_UNROLL = 1000;

int bbLabelCounter = 0;
int slotTrackerNumber = 0;

int main(int argc, char **argv)
{
    // Read the IR file.
    LLVMContext &Context = getGlobalContext();
    SMDiagnostic Err;
    Module *M = ParseIRFile(argv[1], Err, Context);
    CFG * main;
    if (M == nullptr)
    {
      fprintf(stderr, "error: failed to load LLVM IR file \"%s\"", argv[1]);
      return EXIT_FAILURE;
    }
    
    std::map<Instruction*, ValueLong*> variablesToValueMap;
    std::set<BasicBlock*> visitedNode;
    for (auto &F: *M) {
      /* Check to see if the function has a name main. This would be the entry point*/
      if (strncmp(F.getName().str().c_str(),"main",4) == 0){
	  BasicBlock* BB = dyn_cast<BasicBlock>(F.begin());
          /* create the root node of the CFG: main_node */
          main = getNode(BB, "main");
	  BBtoNameMap.insert(std::make_pair(BB, "main"));
          /* detects all the loop blocks and updates the loop blocks. */
          detectAllloopBlocks(BB, visitedNode);
          /* Generates the CFG as well as populates the secretVars */
	  variablesToValueMap = differencePassOnBlock(BB, main, variablesToValueMap);
      }
    }

    /* Finally print the difference of all the variables in the program */
    //outputvariablesTOValueMap(variablesToValueMap);
    //outputvariablesToCrudeIntervalMap();
    outputSeps();
    /* ------------------------- Between here ------------------------  */
}

CFG* getNode(llvm::BasicBlock* BB, std::string bbName){
    /* Function to create a basic block node */
    CFG* newNode = new CFG;
    newNode->name = bbName;
    newNode->pointer = BB;
    return newNode;
}

void detectAllloopBlocks(BasicBlock* BB, 
			 std::set<BasicBlock*> visitedNode){
	/* Function is used to detect all the basic blocks with a while loop*/
        if (BBtoNameMap.find(BB) == BBtoNameMap.end()){
		BBtoNameMap.insert(std::make_pair(BB, getName(BB)));
	}
        //printVisitedBlocks(visitedNode);
        if (visitedNode.find(BB)  == visitedNode.end()){
		visitedNode.insert(BB);
                const TerminatorInst *TInst = BB->getTerminator();
        	int NSucc = TInst->getNumSuccessors();
		for (int i = 0;  i < NSucc; ++i) {
			std::set<BasicBlock*> visitedNodeToChild = visitedNode;
               		BasicBlock *Succ = TInst->getSuccessor(i);
                        detectAllloopBlocks(Succ, visitedNodeToChild);
		}
	}
	else{
		/* Found a loop, hence add it.*/
                if (loopBlocks.find(BB) == loopBlocks.end()){
                	//printf("loop detected");
                        //printf("%s\n", BBtoNameMap.find(BB)->second.c_str());
			loopBlocks.insert(BB);
		}

	}
	return;
      
}

void resetLoopInfo(){
	/* This function resets the loop variable increase and decrease info */
	for(auto it = variableToCrudeIntervalMap.cbegin(); it != variableToCrudeIntervalMap.cend(); ++it){
		if (loopIncreaseInfo.find(it->first) == loopIncreaseInfo.end()){
			loopIncreaseInfo[it->first] = 0;
		} else {
			loopIncreaseInfo.insert(std::make_pair(it->first, 0));		
		}
	}
}

std::map<Instruction*, Interval*> getLatestMap(){
	std::map<Instruction*, Interval*> newMap;
	for(auto it = variableToCrudeIntervalMap.cbegin(); it != variableToCrudeIntervalMap.cend(); ++it){
		Interval* interval = new Interval;
		interval->lowerBound = it->second->lowerBound;
		interval->upperBound = it->second->upperBound;
		newMap.insert(std::make_pair(it->first, interval));
        }
	return newMap;

}

void outputBBToIntervalMap(BasicBlock* BB){
	if (bbToIntervalMap.find(BB) == bbToIntervalMap.end())
		return;
	std::map<Instruction*, Interval*>  map = bbToIntervalMap.find(BB)->second;
	for(auto it = map.cbegin(); it != map.cend(); ++it){
                printf("%s\n", it->first->getName().str().c_str());
                printf("[%ld,%ld]\n", it->second->lowerBound, it->second->upperBound);
	}
	
}

std::map<Instruction*, ValueLong*> getFixedPointOfLoop(BasicBlock* BB, std::map<Instruction*, ValueLong*> variablesToValueMap){
	/* This function gets the fixed point of the block */
	if (loopBlocks.find(BB) != loopBlocks.end()){
		bool isReached = checkIfFixedPointIsReached(BB);
		if (isReached){
			//printf("Fixed point is reached: ");
		} else {
			//printf("fixed point not reached");
			bbToIntervalMap[BB] = getLatestMap();
			/* Only use the loop block BasicBlock and not the terminating one */
			const TerminatorInst *TInst = BB->getTerminator();
			BasicBlock *Succ = TInst->getSuccessor(0);
			variablesToValueMap = getFixedPointOfLoop(Succ, variablesToValueMap);
		}
	} else {
		/* Not a loop block, hence calculate the */
		variablesToValueMap = updateVariablesOfBasicBlock(BB, variablesToValueMap);
		const TerminatorInst *TInst = BB->getTerminator();
                int NSucc = TInst->getNumSuccessors();
		for (int i=0; i<NSucc; ++i){
			BasicBlock *Succ = TInst->getSuccessor(i);
			variablesToValueMap = getFixedPointOfLoop(Succ, variablesToValueMap);
		}
	}
	return variablesToValueMap;
}

void markUnboundedVariablesAfterLoop(BasicBlock *BB){
	/* Function marks all those variables as bounded or unbounded that kept on increasing during the loop */
	long numloops =  bbToLoopUnRollCounter.find(BB)->second;
	for (auto it = loopIncreaseInfo.cbegin(); it != loopIncreaseInfo.cend(); ++it){
		long increase = it->second;
		if (std::abs(increase)/numloops >= 0.9){
			/* Variable is not bounded. Need to mark it*/
			Interval* interval = variableToCrudeIntervalMap.find(it->first)->second;
			if (increase < 0){
				interval->lowerBound = std::numeric_limits<long>::min();
			} else {
				interval->upperBound = std::numeric_limits<long>::max();
			}
		} else {
			/* Variable is not bounded. Dont need to mark it */
		}
	}
}

std::map<Instruction*, ValueLong*> differencePassOnBlock(BasicBlock* BB, 
						   CFG* parentNode, 
						   std::map<Instruction*, ValueLong*> variablesToValueMap){
	
        bool pass = true;
	bool isloop = false;
	bool isFixedPointReached = false;
        if (loopBlocks.find(BB) != loopBlocks.end()) {
		/* A loop is detected and If there is a change in newSecretVars, then pass else Not */

		isLoopContext = true;
		resetLoopInfo();
		variablesToValueMap = getFixedPointOfLoop(BB, variablesToValueMap);
		markUnboundedVariablesAfterLoop(BB);
		resetLoopInfo();
		isLoopContext = false;

		const TerminatorInst *TInst = BB->getTerminator();
		BasicBlock *Succ = TInst->getSuccessor(1);
		CFG* newNode = getNode(Succ, "some block");
                addNode(parentNode, newNode);
		variablesToValueMap = differencePassOnBlock(Succ, newNode, variablesToValueMap);
		
	} else {
		variablesToValueMap = updateVariablesOfBasicBlock(BB, variablesToValueMap);
		const TerminatorInst *TInst = BB->getTerminator();
  		int NSucc = TInst->getNumSuccessors();
			
  		for (int i = 0;  i < NSucc; ++i) {
    			BasicBlock *Succ = TInst->getSuccessor(i);
                	std::map<BasicBlock*, std::string>::iterator it;
                	std::string name;
                	it = BBtoNameMap.find(Succ);
                	if (it != BBtoNameMap.end()){
                		name = it->second;
			}
                	else{
                		name = getName(Succ);
                        	BBtoNameMap.insert(std::make_pair(Succ, name));
                	}
                	//Add the node to BBtoNameMap.
                	CFG* newNode = getNode(Succ, name);
                	addNode(parentNode, newNode);
    			variablesToValueMap = differencePassOnBlock(Succ, newNode, variablesToValueMap);
  		}
                if (NSucc == 0){
			/* Add logic to handle the final Basic Block */
		}
       	}
	return variablesToValueMap;
}

bool isSameInterval(Interval* newInt, Interval* oldInt){
	return (newInt->lowerBound == oldInt->lowerBound) && (newInt->upperBound == oldInt->upperBound);
}

bool anyChangeinRangeofValues(BasicBlock* BB){
	if (bbToIntervalMap.find(BB) == bbToIntervalMap.end()){
		bbToIntervalMap.insert(std::make_pair(BB, getLatestMap()));
		return true;
	} else {
		std::map<Instruction*, Interval*> oldMap = bbToIntervalMap.find(BB)->second;
		for(auto it = variableToCrudeIntervalMap.cbegin(); it != variableToCrudeIntervalMap.cend(); ++it ){
                	Interval* newInt = it->second;
			Interval* oldInt = oldMap.find(it->first)->second;
			if (!isSameInterval(newInt, oldInt)){
				return true;	
			}
        	}
		return false;
		
	}
}

bool isLoopUnrollLimitReached(BasicBlock* BB){
	long currentLoopUnRolls = bbToLoopUnRollCounter.find(BB)->second;
	if (currentLoopUnRolls > MAX_NUMBER_OF_LOOP_UNROLL)
		return true;
	bbToLoopUnRollCounter[BB] = ++currentLoopUnRolls;
	return false;
}

bool checkIfFixedPointIsReached(BasicBlock* BB) {
	if (!anyChangeinRangeofValues(BB) || isLoopUnrollLimitReached(BB)){
		return true;
	}
	return false;
}

/* This is the crux of the block because it handles all the important basic block logic,
   where it goes over all the instruction and updates variables to value map */
std::map<Instruction*, ValueLong*> updateVariablesOfBasicBlock(BasicBlock* BB, std::map<Instruction*, ValueLong*> variablesToValueMap){
	/*Function which checks Leakage of public to private variables*/
        for (auto &I: *BB){
                /* Check for allocate Instruction */
                if (isa<AllocaInst>(I)){
       		         /* Handle allocaInstruction */
                         Instruction* variable = dyn_cast<Instruction>(&I);
                         
                         /* If the variable is unnamed, give it a name and store it in the instaMap */
                         std::string name;
                         if (instNameMap.find(variable) == instNameMap.end()){
                                name = getVariableName(variable);
                                instNameMap.insert(std::make_pair(variable, name));
                         }
                         
                         if (variablesToValueMap.find(variable) == variablesToValueMap.end()){
			 	/* Admit the variable into the map. */
                                ValueLong* val = new ValueLong;
				val->unbounded = true;
				variablesToValueMap.insert(std::make_pair(variable, val));
			 }
			
			 if (variable->hasName())
			 	variablesSet.insert(variable);
		}
                else if (isa<LoadInst>(I)){
                        Instruction* labelInst = dyn_cast<Instruction>(&I);
                        Instruction* op1 = dyn_cast<Instruction>(I.getOperand(0));
                        //printf("%s\n", op1->getName().str().c_str());
                        std::string name;
                        if (instNameMap.find(labelInst) == instNameMap.end()){
                        	name = getVariableName(labelInst);
                                instNameMap.insert(std::make_pair(labelInst, name));
                        }
			ValueLong* val = variablesToValueMap.find(op1)->second;
                        variablesToValueMap = addValueToVariable(val, labelInst, variablesToValueMap);
		}
                else if (isa<StoreInst>(I)){
			/* Found a store Instruction */
                        Value* v = I.getOperand(0);
      			Instruction* op1 = dyn_cast<Instruction>(v);
                        Instruction* op2 = dyn_cast<Instruction>(I.getOperand(1));
                        if (ConstantInt* CI = dyn_cast<ConstantInt>(v)){
                                long val = CI->getSExtValue();
				ValueLong* value = new ValueLong();
				value->min = val;
				value->max = val;
				value->val = val;
				value->unbounded = false;
				variablesToValueMap = addValueToVariable(value, op2, variablesToValueMap);
				if (op2->hasName())
                                	addValueToIntervalMap(value, op2);
	
			} else{
			        Instruction* op1 = dyn_cast<Instruction>(v);
				if (variablesToValueMap.find(op1) == variablesToValueMap.end()){
					printf("Throw error: Variable to value map missing");
				} else {
                                        ValueLong* val = variablesToValueMap.find(op1)->second;
					variablesToValueMap = addValueToVariable(val, op2, variablesToValueMap);
					if (op2->hasName())
						addValueToIntervalMap(val, op2);
				}	
			}
		}
                else if (isa<CmpInst>(I)){
                        /* Do nothing in this case */
		}
                else if (isa<BranchInst>(I)){
			/* Do nothing in this case */
                }
                else if (isa<BinaryOperator>(I)){
                        std::string opcode = I.getOpcodeName();
			Instruction* labelInst = dyn_cast<Instruction>(&I);
                        std::string name;
                        if (instNameMap.find(labelInst) == instNameMap.end()){
                                name = getVariableName(labelInst);
                                instNameMap.insert(std::make_pair(labelInst, name));
                        }
			
			Value* op1 = I.getOperand(0);
			Value* op2 = I.getOperand(1);
			ValueLong* value1 = new ValueLong;
			ValueLong* value2 = new ValueLong;
			long val1, val2;
			
			if (ConstantInt* CI = dyn_cast<ConstantInt>(op1)){
				val1 = CI->getSExtValue();
				value1->min = val1;
				value1->max = val1;
				value1->val = val1;
				value1->unbounded = false;
		 	} else {
				value1 = variablesToValueMap.find(dyn_cast<Instruction>(op1))->second;
			}
			if (ConstantInt* CI = dyn_cast<ConstantInt>(op2)){
                                val2 = CI->getSExtValue();
				value2->min = val2;
				value2->max = val2;
				value2->val = val2;
				value2->unbounded = false;
                        } else {
                                value2 = variablesToValueMap.find(dyn_cast<Instruction>(op2))->second;
                        }
                        ValueLong* expressionValue = executeExpression(opcode, value1, value2);
                        variablesToValueMap = addValueToVariable(expressionValue, labelInst, variablesToValueMap);
                }
		else{
			/* Other Instruction */
		}
        }
	return variablesToValueMap;

}

ValueLong* executeExpression(std::string opCode, ValueLong* v1, ValueLong* v2){
	/* For our case null means unbounded */
	ValueLong *val = new ValueLong;
	val->unbounded = false;
	if (v1->unbounded || v2->unbounded){
		val->unbounded = true;
	}
	long operanda = v1->val;
	long operandb = v2->val;
	if (opCode.compare("mul") == 0){
		long res = operanda * operandb;
		val->min = res;
		val->max = res;
		val->val = res;
	} else if (opCode.compare("add") == 0){
		long res = operanda + operandb;
		val->min = res;
                val->max = res;
                val->val = res;
	} else if (opCode.compare("sub") == 0){
		long res = operanda - operandb;
		val->min = res;
                val->max = res;
                val->val = res;
	} else if (opCode.compare("udiv") == 0){
		long res =  operanda / operandb;
		val->min = res;
                val->max = res;
                val->val = res;
	} else if (opCode.compare("sdiv") == 0){
		long res = operanda / operandb;
		val->min = res;
                val->max = res;
                val->val = res;
	} else if (opCode.compare("urem") == 0){
		long res = operanda % operandb;
		val->min = 0;
                val->max = operandb - 1;
                val->val = res;
	} else if (opCode.compare("srem") == 0){
                long res = operanda % operandb;
		val->min = 0;
                val->max = operandb - 1;
                val->val = res;
        } else if (opCode.compare("shl") == 0){
		long res = operanda << operandb;
		val->min = res;
                val->max = res;
                val->val = res;
	} else if (opCode.compare("lshr") == 0){
		long res =  operanda >> operandb;
		val->min = res;
                val->max = res;
                val->val = res;
	} else if (opCode.compare("ashr") == 0){
		long res = operanda >> operandb;
		val->min = res;
                val->max = res;
                val->val = res;
	} else {
		printf("Unsupported operations");
	}
	return val;
}

void addValueToIntervalMap(ValueLong* val, Instruction* op2){
	/* This function updates the interval of the variables*/
        long lowerBound, upperBound;
	if(val->unbounded){
		lowerBound = std::numeric_limits<long>::min();
		upperBound = std::numeric_limits<long>::max();
	} else {
		lowerBound = val->min;
        	upperBound = val->max;
	}
	if (variableToCrudeIntervalMap.find(op2) == variableToCrudeIntervalMap.end()){
		Interval* interval = new Interval;
		interval->lowerBound = lowerBound;
		interval->upperBound = upperBound;
		variableToCrudeIntervalMap.insert(std::make_pair(op2, interval));
	} else {
		Interval* interval = variableToCrudeIntervalMap.find(op2)->second;
                bool change = false;
		if (lowerBound < interval->lowerBound){
			interval->lowerBound = lowerBound;
			if (isLoopContext){
				long val = loopIncreaseInfo.find(op2)->second;
				loopIncreaseInfo[op2] = --val;
			}
			change = true;
		}
		if (upperBound > interval->upperBound){
			interval->upperBound = upperBound;
			if (isLoopContext){
                                long val = loopIncreaseInfo.find(op2)->second;
                                loopIncreaseInfo[op2] = ++val;
                        }

			change = true;
		} 
		if (change){
			variableToCrudeIntervalMap[op2] = interval;
		}
	}
}

std::map<Instruction*, ValueLong*> addValueToVariable(ValueLong* val, Instruction* op2, std::map<Instruction*, ValueLong*> variablesToValueMap){
	/* This function takes the variablesToValueMap adds the value for the op2 in the variablesToValueMap */
	if (variablesToValueMap.find(op2) == variablesToValueMap.end()){
		variablesToValueMap.insert(std::make_pair(op2, val));
	}else{
		variablesToValueMap[op2] = val;
	}
	return variablesToValueMap;
}

void printVisitedBlocks(std::set<BasicBlock*> blocks){
        /* print the secret Variables */
        std::string name;
        for (auto elem: blocks){
                if (elem->hasName()){
                        name = elem->getName().str();
                }
                else{
                        name = BBtoNameMap.find(elem)->second;
                }
                printf("%s ", name.c_str());
        }
        printf("\n");
}

void sep(Instruction* i, Instruction* j){
        if (variableToCrudeIntervalMap.find(i) == variableToCrudeIntervalMap.end() || variableToCrudeIntervalMap.find(j) == variableToCrudeIntervalMap.end()){
		printf("sep(%s, %s) = %s\n", i->getName().str().c_str(), j->getName().str().c_str(), "Unbounded");
		return;
	}
	Interval* interval_i = variableToCrudeIntervalMap.find(i)->second;
	Interval* interval_j = variableToCrudeIntervalMap.find(j)->second;
	long max = std::max(interval_i->upperBound, interval_j->upperBound);
	long min = std::min(interval_i->lowerBound, interval_j->lowerBound);
	if (max == std::numeric_limits<long>::max())
		printf("sep(%s, %s) = %s\n", i->getName().str().c_str(), j->getName().str().c_str(), "Unbounded");
	else if (min == std::numeric_limits<long>::min())
		printf("sep(%s, %s) = %s\n", i->getName().str().c_str(), j->getName().str().c_str(), "Unbounded");
        
	else 
		printf("sep(%s, %s) = %d\n", i->getName().str().c_str(), j->getName().str().c_str(), max - min);
}

void outputSeps(){
	printf("Printing the separation between the variables --->\n");
	/* Prints the sep between all pairs of variables */
	for (Instruction* inst_i: variablesSet){
		for (Instruction *inst_j: variablesSet){
			if (inst_i->getName().compare(inst_j->getName()) < 0){
				sep(inst_i, inst_j);
			}
		}
	}
}

void outputvariablesTOValueMap(std::map<Instruction*, ValueLong*> variablesToValueMap){
	for(auto it = variablesToValueMap.cbegin(); it != variablesToValueMap.cend(); ++it ){
		std::string name;
		if (it->first->hasName()){
			name = it->first->getName().str();
		} else {
			name = instNameMap.find(it->first)->second;
		}
		printf("name : %s, value: %ld\n", name.c_str(), it->second->val);
	}
}

void outputvariablesToCrudeIntervalMap(){
        for(auto it = variableToCrudeIntervalMap.cbegin(); it != variableToCrudeIntervalMap.cend(); ++it){
                std::string name = it->first->getName().str();
                Interval *interval = variableToCrudeIntervalMap.find(it->first)->second;
                printf("name : %s, Interval: [%ld, %ld] \n", name.c_str(), interval->lowerBound, interval->upperBound);
        }
}


std::string getName(BasicBlock *BB){
	if (BB->hasName()){
        	return BB->getName().str();
        }
        else{
		return std::string("%") + std::to_string(++bbLabelCounter);
        }
}

std::string getVariableName(Instruction* I){
	if (I->hasName()){
		return I->getName().str();
	}
        else{
		return std::string("%") + std::to_string(++slotTrackerNumber);
        }
}

void printCFG(CFG *g){
	if (g == nullptr){
		return;
        }
        printf("Node Name: %s", g->name.c_str());
        for (auto elem: g->children){
        	printCFG(elem);
        }
}

void addNode(CFG* parent, CFG* child){
     /* Function to add a BasicBlock b to the children of g */
     parent->children.insert(child);
}

